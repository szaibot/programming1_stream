package com.sda.programming1.stream.exercise;

/**
 * TODO
 */
public class Book {
    private String title;
    private Author author;
    private int numberOfPages;

    public Book(String title, Author author, int numberOfPages) {
        this.title = title;
        this.author = author;
        this.numberOfPages = numberOfPages;
    }

    public String getTitle() {
        return title;
    }

    public Author getAuthor() {
        return author;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public class Author {
        private String name;
        private String lastName;
        private int age;
        private boolean isAlive;

        public Author(String name, String lastName, int age, boolean isAlive) {
            this.name = name;
            this.lastName = lastName;
            this.age = age;
            this.isAlive = isAlive;
        }

        public String getName() {
            return name;
        }

        public String getLastName() {
            return lastName;
        }

        public int getAge() {
            return age;
        }

        public boolean isAlive() {
            return isAlive;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Author)) return false;

            Author author = (Author) o;

            if (age != author.age) return false;
            if (isAlive != author.isAlive) return false;
            if (name != null ? !name.equals(author.name) : author.name != null) return false;
            return lastName != null ? lastName.equals(author.lastName) : author.lastName == null;
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
            result = 31 * result + age;
            result = 31 * result + (isAlive ? 1 : 0);
            return result;
        }
    }

}
