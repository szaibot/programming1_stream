package com.sda.programming1.stream.exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * TODO
 */
public class StreamExercise {

    /**
     * Metoda zlicza ilość stringów w podanej liście. Użyj streamu nie metody size() ;)
     */
    public long method(List<String> strings) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca listę, która zawierać będzie tylko elementy z wartością 0
     */
    public List<Integer> method0(List<Integer> numbers) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca listę, która zawierać będzie tylko elementy z wartością większą od 7 i mniejsze od 15.
     */
    public List<Integer> method1(List<Integer> numbers) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Przerób metodę poniżej na streama
     */
    public List<Integer> method2(List<Integer> numbers) {
        List<Integer> negativeNumbers = new ArrayList<>();
        for (Integer number : numbers) {
            if (number < 0) {
                negativeNumbers.add(number);
            }
        }

        return negativeNumbers;
    }

    /**
     * Metoda zwraca listę, która zawierać będzie tylko niepuste stringi.
     * Usunąć elementy takie jak  "" lub "     ".
     */
    public List<String> method3(List<String> strings) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca liczbę niepustych stringów.
     */
    public long method4(List<String> strings) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }


    /**
     * Metoda zwraca słowa, które napisane są wielkimi literami. Skorzystaj z metod dostępnych w klasie String.
     */
    public List<String> method5(List<String> strings) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca liczbę stringów zaczynających się od stringa przekazanego jako 2 parametr. Skorzystaj z metod dostępnych w klasie String.
     */
    public long method6(List<String> strings, String preffix) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca słowa, które napisane są z wielkiej litery.
     * Ogranicz ilość elementów do 15.
     */
    public List<String> method7(List<String> strings) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda sprawdza czy lista zawiera chociaż jeden element taki sam jak drugi parametr.
     * Hint użyj innej metody zamykającej stream niż collect i count.
     */
    public boolean method8(List<String> strings, String pattern) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda sprawdza czy wszystkie elementy rozpoczynają sie od stringa przekazanego jako 2 parametr.
     * Skorzystaj z metod dostępnych w klasie String.
     * Hint użyj innej metody zamykającej stream niż collect i count.
     */
    public boolean method9(List<String> strings, String pattern) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zamienia liste stringów, na liste intów odpowiadających długości znaków.
     * Przykład
     * wejściowe -> ["pierwszy", "drugi"]
     * wyjściowe -> [8, 5]
     */
    public List<Integer> method10(List<String> strings) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zamienia elementy, które są takie same jak 2 parametr, na 3 parametr.
     * Skorzystaj z metod dostępnych w klasie String.
     */
    public List<String> method11(List<String> strings, String pattern, String newValue) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda usuwa puste znaki z początku i końca stringu
     * Skorzystaj z metod dostępnych w klasie String.
     * Przykład
     * wejściowe -> ["      pierwszy        ", "         drugi        "]
     * wyjściowe -> ["pierwszy", "drugi"]
     */
    public List<String> method12(List<String> strings, String pattern, String newValue) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zamienia litery na duże.
     * Skorzystaj z metod dostępnych w klasie String.
     * Przykład
     * wejściowe -> ["pierwszy", "Drugi"]
     * wyjściowe -> ["PIERWSZY", "DRUGI"]
     */
    public List<String> method13(List<String> strings, String pattern, String newValue) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Kolejne testy operować będą na klasie Book zdefiniowanej w tym samym pakiecie
     */

    /**
     * Metoda zwraca tylko takie książki, które mają więcej niż 200 stron
     */
    public List<Book> method14(List<Book> books) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca tylko takie książki, które mają tytuł taki jak drugi parametr
     */
    public List<Book> method15(List<Book> books, String title) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca wszystkich autorów, bez duplikatów
     */
    public List<Book.Author> method16(List<Book> books) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca wszystkich autorów, bez duplikatów, którzy mają więcej niż 30 lat
     */
    public List<Book.Author> method17(List<Book> books) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca liczbę autorów, którzy aktualnie żyją. Usuń duplikaty
     */
    public long method18(List<Book> books) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca połączone imię i nazwisko wszystkich autorów
     * Przykład
     * wejście -> Books[
     *                  ["Pan Tadeusz", Author["Adam", "Mickiewicz"]]
     *                  ["Quo Vadis", Author["Henryk", "Sienkiewicz"]]
     *                 ]
     * wyjscie -> ["Adam Mickiewicz", "Henryk Sienkiewicz"]
     */
    public List<String> method19(List<Book> books) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca imiona autorów którzy umarli w wieku większym niż 30 lat (isAlive - false i age >30) i mają nazwisko zaczynające się na "K"
     * i są autorami książek które są dłuższe niż 50 stron.
     */
    public List<String> method20(List<Book> books) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }

    /**
     * Metoda zwraca mapę nazwa książki -> nazwisko autora
     * Przykład
     * wejście -> Books[
     *                  ["Pan Tadeusz", Author["Adam", "Mickiewicz"]]
     *                  ["Quo Vadis", Author["Henryk", "Sienkiewicz"]]
     *                 ]
     * wyjscie -> [
     *              ["Pan Tadeusz", "Mickiewicz",
     *               "Quo Vadis", "Sienkiewicz"]
     *            ]
     *
     * HINT należy użyć innej metody w metodzie collect()
     */
    public Map<String, String> method21(List<Book> books) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }


    /**
     * Metoda zwraca listę tytułów. Każdy z tytułów skróć do max 3 słów
     */
    public List<String> method22(List<Book> books) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED!!");
    }
}
