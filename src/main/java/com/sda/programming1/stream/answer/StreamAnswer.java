package com.sda.programming1.stream.answer;

import com.sda.programming1.stream.exercise.Book;
import com.sda.programming1.stream.exercise.StreamExercise;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * TODO
 */
class StreamAnswer extends StreamExercise {

    @Override
    public long method(List<String> strings) {
        return strings.stream().count();
    }

    @Override
    public List<Integer> method0(List<Integer> numbers) {
        return numbers.stream()
                .filter(number -> number == 0)
                .collect(Collectors.toList());
    }

    @Override
    public List<Integer> method1(List<Integer> numbers) {
        return numbers.stream()
                .filter(number -> number > 7 && number < 15)
                .collect(Collectors.toList());
    }

    @Override
    public List<Integer> method2(List<Integer> numbers) {
        return numbers.stream()
                .filter(number -> number < 0)
                .collect(Collectors.toList());
    }

    @Override
    public List<String> method3(List<String> strings) {
        return strings.stream()
                .filter(s -> !s.isEmpty())
                .collect(Collectors.toList());
    }

    @Override
    public long method4(List<String> strings) {
        return strings.stream()
                .filter(s -> !s.isEmpty())
                .count();
    }

    @Override
    public List<String> method5(List<String> strings) {
        return strings.stream()
                .filter(s -> s.equals(s.toUpperCase()))
                .collect(Collectors.toList());
    }

    @Override
    public long method6(List<String> strings, String preffix) {
        return strings.stream()
                .filter(s -> s.startsWith(preffix))
                .count();
    }

    @Override
    public List<String> method7(List<String> strings) {
        return strings.stream()
                .filter(s -> s.equals(Character.toUpperCase(s.charAt(0)) + s.substring(1)))
                .collect(Collectors.toList());
    }

    @Override
    public boolean method8(List<String> strings, String pattern) {
        return strings.stream()
                .anyMatch(s -> s.equals(pattern));
    }

    @Override
    public boolean method9(List<String> strings, String pattern) {
        return strings.stream()
                .allMatch(s -> s.startsWith(pattern));
    }

    @Override
    public List<Integer> method10(List<String> strings) {
        return strings.stream()
                .map(String::length)
                .collect(Collectors.toList());
    }

    @Override
    public List<String> method11(List<String> strings, String pattern, String newValue) {
        return strings.stream()
                .map(s -> s.replaceAll(pattern, newValue))
                .collect(Collectors.toList());
    }

    @Override
    public List<String> method12(List<String> strings, String pattern, String newValue) {
        return strings.stream()
                .map(s -> s.trim())
                .collect(Collectors.toList());
    }

    @Override
    public List<String> method13(List<String> strings, String pattern, String newValue) {
        return strings.stream()
                .map(s -> s.toUpperCase())
                .collect(Collectors.toList());
    }

    @Override
    public List<Book> method14(List<Book> books) {
        return books.stream()
                .filter(book -> book.getNumberOfPages() > 200)
                .collect(Collectors.toList());
    }

    @Override
    public List<Book> method15(List<Book> books, String title) {
        return books.stream()
                .filter(book -> book.getTitle().equals(title))
                .collect(Collectors.toList());
    }

    @Override
    public List<Book.Author> method16(List<Book> books) {
        return books.stream()
                .map(book -> book.getAuthor())
                .distinct()
                .collect(Collectors.toList());
    }

    @Override
    public List<Book.Author> method17(List<Book> books) {
        return books.stream()
                .map(book -> book.getAuthor())
                .filter(author -> author.getAge() > 30)
                .collect(Collectors.toList());
    }

    @Override
    public long method18(List<Book> books) {
        return books.stream()
                .map(book -> book.getAuthor())
                .filter(author -> author.isAlive())
                .count();
    }

    @Override
    public List<String> method19(List<Book> books) {
        return books.stream()
                .map(book -> book.getAuthor().getName() + " " +  book.getAuthor().getLastName())
                .collect(Collectors.toList());
    }

    @Override
    public List<String> method20(List<Book> books) {
        return books.stream()
                .filter(book -> book.getNumberOfPages() > 50)
                .map(book -> book.getAuthor())
                .filter(author -> !author.isAlive())
                .filter(author -> author.getAge() > 30)
                .filter(author -> author.getLastName().startsWith("K"))
                .map(author -> author.getName())
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, String> method21(List<Book> books) {
        return books.stream()
                .collect(Collectors.toMap(
                        book -> book.getTitle(),
                        book -> book.getAuthor().getLastName()
                ));
    }


    /**
     * Metoda zwraca listę tytułów. Każdy z tytułów skróć do max 3 słów
     */
    @Override
    public List<String> method22(List<Book> books) {
        return books.stream()
                .map(book -> Arrays.stream(book.getTitle()
                                .split("\\s+"))
                                .limit(3)
                                .collect(Collectors.joining()))
                .collect(Collectors.toList());
    }
}
