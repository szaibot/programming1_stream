package com.sda.programming1.lambda.anonymous.exercise;

public class FindYourselfExercise {

    public static void main(String[] args) {
        A a = new A();

/*
    TODO zaimplementuj metodę multiply1 w klasie A i uruchom program.
    TODO Odkomentuj kod znajdujący się w klasie A (poniżej) dla danego zadania, napraw błędy i zamień wywołania a.multiply1 na a.multiply2 itd.

    HINT: skrót w IntelliJ do odkomentowywania bloków kodu - zaznacz fragment kodu i naciśnij Ctrl + /

    W klasie answer/FindYourselfAnswer macie przykładowe odpowiedzi.
    Czasami IntelliJ będzie starał się skracać rozwiązania (tylko wizualnie) szczególnie w rozwiązaniach z klasą anonimową - zamienia je na lambdy.
    w takim przypadku naciśnijcie plusik po lewej stronie obok numeru linii, wtedy rozwinie wam prawdziwe rozwiązanie.
    Niestety IntelliJ nie rozumie, że chciałem jawnie zmniejszyć czytelność kodu :D
*/

         int result = a.multiply1(2, 2);
         int result2 = a.multiply1(0, 2);


        if (result == 4) {
            System.out.println("Poprawny wynik");
        } else {
            System.out.println("Niepoprawny wynik, oczekiwana wartość=" + 4 + " aktualna=" + result);
            throw new IllegalStateException("METODA ZOSTALA ZLE ZAIMPLEMENTOWANA");
        }

        
        if (result2 == 0) {
            System.out.println("Poprawny wynik");
        } else {
            System.out.println("Niepoprawny wynik, oczekiwana wartość=" + 0 + " aktualna=" + result);
            throw new IllegalStateException("METODA ZOSTALA ZLE ZAIMPLEMENTOWANA");
        }
    }


static class A {

    interface Multiplier {
        int multiply(int x, int y);
    }

    //************************************************ EXERCISE 1

    int multiply1(int x, int y) {
        throw new UnsupportedOperationException("missing implementation");
    }

//    //************************************************ EXERCISE 2
//
//    int multiply2(int x, int y) {
//        if (x == 0) {
//            //EXERCISE 2
//        } else {
//            //EXERCISE 2
//        }
//    }
//
//    //************************************************ EXERCISE 3
//
//    int multiply3(int x, int y) {
//        if (x == 0) { //EXERCISE 3
//        }
//    else { /*EXERCISE 3*/
//    }
//    }
//
//    //************************************************ EXERCISE 4
//
//    int multiply4(int x, int y) {
//        if (x == 0) {
//                                }
//                    else {
//        }
//    }
//
//    //************************************************ EXERCISE 5
//
//    int multiply5(int x, int y) {
//        if (x == 0) {
//                                        }
//
//
//
//
//                        else            {
//        }
//    }
//
//    //************************************************ EXERCISE 6
//
//    int multiply6(int x, int y) {
//        int result = 0;
//        for(int i=0; i <10; i++) {
//            if (x == 0) { //EXERCISE 6
//            }
//            else { /*EXERCISE 6*/ }
//        }
//
//        return result;
//     }
//
//
//    //************************************************ EXERCISE 7
//
//    int multiply7(int x, int y){
//        for (int i = 0; i < 10; i++) {
//            if (x == 0) { //EXERCISE 7
//            } else { /*EXERCISE 7*/ }
//        }
//        }
//
//    //************************************************ EXERCISE 8
//
//    int multiply8(int x, int y){
//    for (int i = 0; i < 10; i++) {
//    if (x == 0) { //EXERCISE 8
//                            }
//        else { /*EXERCISE 8*/
//    }
//    }
//    }
//
//    //************************************************ EXERCISE 9
//
//    int multiply9(int x, int y){
//        for (int i = 0; i < 10; i++) {
//            for (int j = 0; j < 10; j++) {
//                if (x == 0) { //EXERCISE 9
//                } else { /*EXERCISE 9*/ }
//            }
//        }
//    }
//
//    //************************************************ EXERCISE 10
//
//    int multiply10(int x, int y){
//    for (int i = 0; i < 10; i++) {
//    for (int j = 0; j < 10; j++) {
//    if (x == 0) { //EXERCISE 10
//    } else { /*EXERCISE 10*/ }
//    }
//    }
//    }
//
//    //************************************************ EXERCISE 11
//
//    int multiply11(int x, int y) {
//        Multiplier multiplier = new Multiplier() {
//            public int multiply(int innerX, int innerY) {
//                //EXERCISE 11
//            }
//        };
//
//        return multiplier.multiply(/*EXERCISE 11*/)
//    }
//
//    //************************************************ EXERCISE 12
//
//    int multiply12(int x, int y) {
//        Multiplier multiplier = new Multiplier() {public int multiply(int innerX, int innerY) {
//                //EXERCISE 12
//        }
//        };
//
//        return //EXERCISE 12
//    }
//
//    //************************************************ EXERCISE 13
//
//    int multiply13(int x, int y) {
//        Multiplier multiplier = new Multiplier() {
//        public int multiply(int x, int y) {//EXERCISE 13
//                                                        }
//        };
//
//        return //EXERCISE 13
//    }
//
//    //************************************************ EXERCISE 14
//
//    int multiply14(int x, int y) {
//        Multiplier multiplier = new Multiplier() {
//            public int multiply(int x, int y) {
//                return /*EXERCISE 14*/;
//            }
//        }
//
//        return multiply14b(multiplier, x, y);
//    }
//
//    private int multiply14b(Multiplier multiplier, int x, int y) {
//        return multiplier.multiply(/*EXERCISE 14*/);
//    }
//
//    //************************************************ EXERCISE 15
//
//    int multiply15(int x, int y) {
//        Multiplier multiplier = /*EXERCISE 15*/;
//
//        return multiply15b(multiplier, x, y);
//    }
//
//    private int multiply15b(Multiplier multiplier, int x, int y) {
//        return multiplier.multiply(/*EXERCISE 15*/);
//    }
//
//    //************************************************ EXERCISE 16
//
//    int multiply16(int x, int y) {
//        return multiply16b(/*EXERCISE 16*/);
//    }
//
//    private int multiply16b(Multiplier multiplier, int x, int y) {
//        return multiplier.multiply(/*EXERCISE 16*/)
//    }
//
//
//    //************************************************ EXERCISE 17
//
//    int multiply17(int x, int y) {
//        Multiplier multiplier = new Multiplier() {
//            public int multiply(int x, int y) {
//                return /*EXERCISE 17*/;
//            }
//        }
//
//        return multiply17b(multiplier, /*EXERCISE 17*/)
//    }
//
//    private int multiply17b(final Multiplier multiplier, int x, int y) {
//        Multiplier multiplier1 = new Multiplier() {
//            public int multiply(int x, int y) {
//                multiplier.multiply(/*EXERCISE 17*/)
//            }
//        }
//
//        return multiplier1.multiply(x, y);
//    }
//
//    //************************************************ EXERCISE 18
//
//    int multiply18(int x, int y) {
//        Multiplier multiplier = /*EXERCISE 18*/;
//
//        return multiply18b(multiplier, /*EXERCISE 18*/)
//    }
//
//    private int multiply18b(final Multiplier multiplier, int x, int y) {
//        Multiplier multiplier1 = new Multiplier() {
//            public int multiply(int x, int y) {
//                /*EXERCISE 18*/
//            }
//        }
//
//        return multiplier1.multiply(x, y);
//    }
//
//    //************************************************ EXERCISE 19
//
//    int multiply19(int x, int y) {
//        return multiply19b(/*EXERCISE 19*/)
//    }
//
//    private int multiply19b(final Multiplier multiplier, int x, int y) {
//        Multiplier multiplier1 = new Multiplier() {
//            public int multiply(int x, int y) {
//                /*EXERCISE 19*/
//            }
//        }
//
//        return multiplier1.multiply(x, y);
//    }
//
//    //************************************************ EXERCISE 20
//
//    int multiply20(int x, int y) {
//        return multiply20b(/*EXERCISE 20*/)
//    }
//
//    private int multiply20b(final Multiplier multiplier, int x, int y) {
//        new Multiplier() {
//            public int multiply(int x, int y) {
//                /*EXERCISE 20*/
//            }
//        }
//
//    }
//
//    //************************************************ EXERCISE 21
//
//    int multiply21(int x, int y) {
//        Multiplier multiplier = /*EXERCISE 21*/;
//
//        return multiply21b(multiplier, /*EXERCISE21*/)
//    }
//
//    private int multiply21b(final Multiplier multiplier, int x, int y) {
//        Multiplier multiplier1 = new Multiplier() {
//            public int multiply(int x, int y) {
//                /*EXERCISE 21*/
//    }}}
//
//
//    //************************************************ EXERCISE 22
//
//
//    int multiply22(int x, int y) {
//        return multiply22b(/*EXERCISE 22*/);
//    }
//
//    private int multiply22b(Multiplier multiplier, int x, int y) {
//        Multiplier multiplier1 = new Multiplier() {
//            public int multiply(int x, int y) {
//                Multiplier innerMultiplier = new Multiplier() {
//                    public int multiply(int x, int y) {
//                        return //EXERCISE 22;
//                    }
//                }
//
//                return //EXERCISE 22;
//            }
//        };
//
//        return multiplier1.multiply();
//    }
//
//    //************************************************ EXERCISE 23
//
//    int multiply23(int x, int y) {
//        return multiply23b(/*EXERCISE 23*/);
//    }
//
//    private int multiply23b(Multiplier multiplier, int x, int y) {
//        Multiplier multiplier1 = new Multiplier() {
//            public int multiply(int x, int y) {
//                Multiplier innerMultiplier = new Multiplier() {
//                    public int multiply(int x, int y) {
//                        return //EXERCISE 23;
//                    }
//                }
//                return //EXERCISE 23;
//            }
//        };
//    }
//
//    //************************************************ EXERCISE 24
//
//
//    int multiply24(int x, int y) {
//        return multiply24b(/*EXERCISE 24*/);
//    }
//
//    private int multiply24b(Multiplier multiplier, int x, int y) {
//        Multiplier multiplier1 = new Multiplier() {
//            public int multiply(int x, int y) {
//                Multiplier innerMultiplier = new Multiplier() {
//                    public int multiply(int x, int y) {
//                    }
//                }
//            }
//        };
//    }
//
//
//    //************************************************ EXERCISE 25
//
//
//    int multiply25(int x, int y) {
//        return multiply25b();
//    }
//
//    private int multiply25b(Multiplier multiplier, int x, int y) {
//        Multiplier multiplier1 = new Multiplier() { public int multiply(int x, int y) { Multiplier innerMultiplier = new Multiplier() {
//        public int multiply(int x, int y) {
//        return}}return}};
//        return multiplier1.multiply();
//    }
}
}
