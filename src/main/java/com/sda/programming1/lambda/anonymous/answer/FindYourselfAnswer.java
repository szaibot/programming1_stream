package com.sda.programming1.lambda.anonymous.answer;

public class FindYourselfAnswer {

    public static void main(String[] args) {
        A a = new A();

        // TODO zaimplementuj metodę multiply1 w klasie A i uruchom program.
        // TODO Odkomentuj kod znajdujący się w klasie A (poniżej) dla danego zadania, napraw błędy i zamień wywołania a.multiply1 na a.multiply2 itd.

        int result = a.multiply25(2, 2);
        int result2 = a.multiply25(0, 2);


        if (result == 4) {
            System.out.println("Poprawny wynik");
        } else {
            System.out.println("Niepoprawny wynik, oczekiwana wartość=" + 4 + " aktualna=" + result);
            throw new IllegalStateException("METODA ZOSTALA ZLE ZAIMPLEMENTOWANA");
        }


        if (result2 == 0) {
            System.out.println("Poprawny wynik");
        } else {
            System.out.println("Niepoprawny wynik, oczekiwana wartość=" + 0 + " aktualna=" + result);
            throw new IllegalStateException("METODA ZOSTALA ZLE ZAIMPLEMENTOWANA");
        }
    }


static class A {

    interface Multiplier {
        int multiply(int x, int y);
    }

    //************************************************ EXERCISE 1

    int multiply1(int x, int y) {
        return x * y;
    }

    //************************************************ EXERCISE 2

    int multiply2(int x, int y) {
        if (x == 0) {
            return 0;
        } else {
            return x * y;
        }
    }

    //************************************************ EXERCISE 3

    int multiply3(int x, int y) {
        if (x == 0) {
            return 0;
        }
    else {
        return x * y;
    }
    }

    //************************************************ EXERCISE 4

    int multiply4(int x, int y) {
        if (x == 0) { return 0;
                                }
                    else {
                        return x * y;
        }
    }

    //************************************************ EXERCISE 5

    int multiply5(int x, int y) {
        if (x == 0) {
            return 0;
                                        }




                        else            {
                            return x * y;
        }
    }

    //************************************************ EXERCISE 6

    int multiply6(int x, int y) {
        int result = 0;

        for(int i=0; i <10; i++) {
            if (x == 0) {
                result = 0;
            }
            else {
                result =  x * y;
            }
        }

        return result;
     }


    //************************************************ EXERCISE 7

    int multiply7(int x, int y) {
        int result = 0;
        for (int i = 0; i < 10; i++) {
            if (x == 0) {
                result = 0;
            } else { result = x * y; }
        }
        return result;
        }

    //************************************************ EXERCISE 8

    int multiply8(int x, int y){
    int result = 0;
    for (int i = 0; i < 10; i++) {
    if (x == 0) {
        result = 0;
                            }
        else { result = x * y;
    }
    }
    return result;
    }

    //************************************************ EXERCISE 9

    int multiply9(int x, int y){
        int result = 0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (x == 0) { result = 0;
                } else { result = x * y; }
            }
        }
        return result;
    }

    //************************************************ EXERCISE 10

    int multiply10(int x, int y){
    int result = 0;
    for (int i = 0; i < 10; i++) {
    for (int j = 0; j < 10; j++) {
    if (x == 0) { result = 0;
    } else { result = x * y; }
    }
    }
    return result;
    }

    //************************************************ EXERCISE 11

    int multiply11(int x, int y) {
        Multiplier multiplier = new Multiplier() {
            public int multiply(int innerX, int innerY) {
                return innerX * innerY;
            }
        };

        return multiplier.multiply(x, y);
    }

    //************************************************ EXERCISE 12

    int multiply12(int x, int y) {
        Multiplier multiplier = new Multiplier() {public int multiply(int x, int y) {
                return x * y;
        }
        };

        return multiplier.multiply(x, y);
    }

    //************************************************ EXERCISE 13

    int multiply13(int x, int y) {
        Multiplier multiplier = new Multiplier() {
        public int multiply(int x, int y) {
                                           return x * y;             }
        };

        return multiplier.multiply(x, y);
    }

    //************************************************ EXERCISE 14

    int multiply14(int x, int y) {
        Multiplier multiplier = new Multiplier() {
            public int multiply(int x, int y) {
                return x * y;
            }
        };

        return multiply14b(multiplier, x, y);
    }

    private int multiply14b(Multiplier multiplier, int x, int y) {
        return multiplier.multiply(x, y);
    }

    //************************************************ EXERCISE 15

    int multiply15(int x, int y) {
        Multiplier multiplier = new Multiplier() {
            public int multiply(int x, int y) {
                return x * y;
            }
        };

        return multiply15b(multiplier, x, y);
    }

    private int multiply15b(Multiplier multiplier, int x, int y) {
        return multiplier.multiply(x, y);
    }

    //************************************************ EXERCISE 16

    int multiply16(int x, int y) {
        Multiplier multiplier = new Multiplier() {
            public int multiply(int x, int y) {
                return x * y;
            }
        };

        return multiply16b(multiplier, x, y);
    }

    private int multiply16b(Multiplier multiplier, int x, int y) {
        return multiplier.multiply(x, y);
    }


    //************************************************ EXERCISE 17

    int multiply17(int x, int y) {
        Multiplier multiplier = new Multiplier() {
            public int multiply(int x, int y) {
                return x * y;
            }
        };

        return multiply17b(multiplier, x, y);
    }

    private int multiply17b(final Multiplier multiplier, int x, int y) {
        Multiplier multiplier1 = new Multiplier() {
            public int multiply(int x, int y) {
                return multiplier.multiply(x, y);
            }
        };

        return multiplier1.multiply(x, y);
    }

    //************************************************ EXERCISE 18

    int multiply18(int x, int y) {
        Multiplier multiplier = new Multiplier() {
            public int multiply(int x, int y) {
                return x * y;
            }
        };

        return multiply18b(multiplier, x, y);
    }

    private int multiply18b(final Multiplier multiplier, int x, int y) {
        Multiplier multiplier1 = new Multiplier() {
            public int multiply(int x, int y) {
                return multiplier.multiply(x, y);
            }
        };

        return multiplier1.multiply(x, y);
    }

    //************************************************ EXERCISE 19

    int multiply19(int x, int y) {
        Multiplier multiplier = new Multiplier() {
            public int multiply(int x, int y) {
                return x * y;
            }
        };

        return multiply19b(multiplier, x, y);
    }

    private int multiply19b(final Multiplier multiplier, int x, int y) {
        Multiplier multiplier1 = new Multiplier() {
            public int multiply(int x, int y) {
                return multiplier.multiply(x, y);
            }
        };

        return multiplier1.multiply(x, y);
    }

    //************************************************ EXERCISE 20

    int multiply20(int x, int y) {
        Multiplier multiplier = new Multiplier() {
            public int multiply(int x, int y) {
                return x * y;
            }
        };

        return multiply20b(multiplier, x, y);
    }

    private int multiply20b(final Multiplier multiplier, int x, int y) {
        Multiplier multiplier1 = new Multiplier() {
            public int multiply(int x, int y) {
                return multiplier.multiply(x, y);
            }
        };

        return multiplier1.multiply(x, y);
    }

    //************************************************ EXERCISE 21

    int multiply21(int x, int y) {
        Multiplier multiplier = new Multiplier() {
            public int multiply(int x, int y) {
                return x * y;
            }
        };

        return multiply21b(multiplier, x, y);
    }

    private int multiply21b(final Multiplier multiplier, int x, int y) {
        Multiplier multiplier1 = new Multiplier() {
            public int multiply(int x, int y) {
                return multiplier.multiply(x, y);
    }};
    return multiplier1.multiply(x, y);}


    //************************************************ EXERCISE 22


    int multiply22(int x, int y) {
        Multiplier multiplier = new Multiplier() {
            public int multiply(int x, int y) {
                return x * y;
            }
        };

        return multiply22b(multiplier, x, y);
    }

    private int multiply22b(final Multiplier multiplier, int x, int y) {
        final Multiplier multiplier1 = new Multiplier() {
            public int multiply(int x, int y) {
                Multiplier innerMultiplier = new Multiplier() {
                    public int multiply(int x, int y) {
                        return multiplier.multiply(x, y);
                    }
                };

                return innerMultiplier.multiply(x, y);
            }
        };

        return multiplier1.multiply(x, y);
    }

    //************************************************ EXERCISE 23

    int multiply23(int x, int y) {
        Multiplier multiplier = new Multiplier() {
            public int multiply(int x, int y) {
                return x * y;
            }
        };

        return multiply23b(multiplier, x, y);
    }

    private int multiply23b(final Multiplier multiplier, int x, int y) {
        Multiplier multiplier1 = new Multiplier() {
            public int multiply(int x, int y) {
                Multiplier innerMultiplier = new Multiplier() {
                    public int multiply(int x, int y) {
                        return multiplier.multiply(x, y);
                    }
                };
                return innerMultiplier.multiply(x, y);
            }
        };

        return multiplier1.multiply(x, y);
    }

    //************************************************ EXERCISE 24


    int multiply24(int x, int y) {
        Multiplier multiplier = new Multiplier() {
            public int multiply(int x, int y) {
                return x * y;
            }
        };

        return multiply24b(multiplier, x, y);
    }

    private int multiply24b(final Multiplier multiplier, int x, int y) {
        Multiplier multiplier1 = new Multiplier() {
            public int multiply(int x, int y) {
                Multiplier innerMultiplier = new Multiplier() {
                    public int multiply(int x, int y) {
                        return multiplier.multiply(x, y);
                    }
                };
                return innerMultiplier.multiply(x, y);
            }
        };

        return multiplier1.multiply(x, y);
    }


    //************************************************ EXERCISE 25


    int multiply25(int x, int y) {
        Multiplier multiplier = new Multiplier() {
            public int multiply(int x, int y) {
                return x * y;
            }
        };

        return multiply25b(multiplier, x, y);
    }

    private int multiply25b(final Multiplier multiplier, int x, int y) {
        Multiplier multiplier1 = new Multiplier() { public int multiply(int x, int y) { Multiplier innerMultiplier = new Multiplier() {
        public int multiply(int x, int y) {
        return multiplier.multiply(x, y);}}; return innerMultiplier.multiply(x, y);}};
        return multiplier1.multiply(x,y);
    }
}
}